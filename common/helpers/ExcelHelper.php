<?php

namespace common\helpers;

use common\models\WithdrawModel;
use common\services\CustomerService;
use PHPExcel_Cell_DataType;
use PHPExcel_Style_NumberFormat;
use PhpOffice\PhpSpreadsheet\IOFactory;
use PHPUnit\Framework\Constraint\Count;
use  PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Style\Alignment;
use PhpOffice\PhpSpreadsheet\Style\Border;

class  ExcelHelper
{

    public static function getWithdrawSheetData($file_path)
    {
        $arr = [];
        if (!file_exists($file_path)) {
            $arr['result']  = false;
            $arr['message'] = "当前文件不存在。无法获取";
            $arr['data']    = '';

            return $arr;
        }

        //创建文件读取类对象
        $reader = IOFactory::createReader("Xlsx");
        /**  Advise the Reader that we only want to load cell data  **/
        //设置为只读模式$reader->setReadDataOnly(true);
        /**  Load $inputFileName to a Spreadsheet Object  **/
        //加载文件路径，路径为绝对路径
        $spreadsheet = $reader->load($file_path);
        //获取excel表中有多少个sheet
        $sheetNum  = $spreadsheet->getSheetCount();
        $sheetData = [];
        //只获取活跃的sheet
        //$temp = $spreadsheet->getSheetActive()->toArray(null, true, true, true);
        //遍历每一个sheet，获取里面的数据内容
        for ($i = 0; $i < $sheetNum; $i++) {
            $temp = $spreadsheet->getSheet($i)->toArray(null, true, true, true);

            if (count($temp) > 1000) {
                $arr['result']  = false;
                $arr['message'] = "上传报表数据异常：当前表格数据不能超过1000行. ";
                $arr['data']    = '';

                return $arr;

            }
            foreach ($temp as $row) {

                if ($row['B'] == 'id' && $row['C'] == '姓名') { //如果是title 则略过
                    continue;
                }
                $tmp['withdraw_no']               = $row['A'];  //用户ID
                $tmp['withdraw_customer_id']      = $row['B'];  //用户ID
                $tmp['withdraw_customer_name']    = $row['C'];  //用户姓名
                $tmp['withdraw_customer_mobile']  = $row['D'];  //用户电话
                $tmp['availableBalance']          = $row['E'];  //可用余额
                $tmp['withdraw_real_amount']      = $row['F'];  //提现金额
                $tmp['withdraw_customer_bank_no'] = $row['G'];  //提现银行账号
                $tmp['withdraw_status']           = $row['H'];  //提现银行账号


                $has_withdraw_no = WithdrawModel::findOne(['withdraw_no' => $tmp['withdraw_no']]);

                if (!empty($has_withdraw_no)) {
                    $arr['result']  = false;
                    $arr['message'] = "上传报表数据异常：【" . $tmp['withdraw_no'] . '-' . '-' . $tmp['withdraw_customer_id'] . '-' . $tmp['withdraw_customer_name'] . '】的提现编号已存在。无法进行导入操作';
                    $arr['data']    = '';

                    return $arr;
                }


                if ($tmp['withdraw_real_amount'] < 0) {
                    $arr['result']  = false;
                    $arr['message'] = "上传报表数据异常：【" . $tmp['withdraw_no'] . '-' . '-' . $tmp['withdraw_customer_id'] . '-' . $tmp['withdraw_customer_name'] . '】的提现金额不能小于0。无法进行导入操作';
                    $arr['data']    = '';

                    return $arr;
                }


                if ($tmp['withdraw_real_amount'] > $tmp['availableBalance']) {
                    $arr['result']  = false;
                    $arr['message'] = "上传报表数据异常：【" . $tmp['withdraw_customer_id'] . '-' . $tmp['withdraw_customer_name'] . '】,转账金额大于用户可用余额';
                    $arr['data']    = '';

                    return $arr;
                }


                //判断用户id 和电话号码是否存在

                $checkResult = (new  CustomerService)->checkCustomer($tmp['withdraw_customer_id'], $tmp['withdraw_customer_mobile']);
                if (!$checkResult) {
                    $arr['result']  = false;
                    $arr['message'] = "上传报表数据异常：当前用户【" . $tmp['withdraw_customer_id'] . '-' . $tmp['withdraw_customer_name'] . '】不存在！！（id及电话无法匹配）';
                    $arr['data']    = '';

                    return $arr;
                }


                $sheetData [] = $tmp;
            }
        }

        $arr['result']  = true;
        $arr['message'] = '成功';
        $arr['data']    = $sheetData;

        return $arr;
    }


    /**
     * 填充数据 输出excel文件
     *
     * @param     $fileName
     * @param     $sheetTitle
     * @param     $rowHeader
     * @param     $excelData
     * @param int $excelLength
     *
     * @return void
     */
    public static function getExcel($fileName, $sheetTitle, $rowHeader, $excelData, $excelLength = 0)
    {
        $excelTag = ['A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z'];

        if (count($rowHeader) > 26) {
            echo "Excel title无法设置，无法导出数据超过26列的数据";
            die();
        }

        if ($excelLength > 0 && count($excelData) > $excelLength) {
            echo "数据长度超过excel设置的单页数据最大值";
            die();
        }

        $spreadsheet = new Spreadsheet();
        $spreadsheet->getProperties()->setCreator('Maarten Balliauw')
            ->setLastModifiedBy('Maarten Balliauw')
            ->setTitle('Office 2007 XLSX Test Document')
            ->setSubject('Office 2007 XLSX Test Document')
            ->setDescription('Test document for Office 2007 XLSX, generated using PHP classes.')
            ->setKeywords('office 2007 openxml php')
            ->setCategory('Test result file');

        // 添加表头
        $header = $spreadsheet->setActiveSheetIndex(0);
        for ($i = 0; $i < count($rowHeader); $i++) {
            $spreadsheet->getActiveSheet()->getColumnDimension($excelTag[$i])->setAutoSize(true);//设置列宽自动
            $spreadsheet->getActiveSheet()->getStyle($excelTag[$i])->getNumberFormat()->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_TEXT);//设置每列为文本格式
            $pCoordinate = $excelTag[$i] . 1;
            $header->setCellValue($pCoordinate, $rowHeader[$i]);
        }

        //        添加表内容
        $body = $spreadsheet->setActiveSheetIndex(0);
        for ($j = 0; $j < count($excelData); $j++) {
            $tmp1 = $j + 2;
            for ($k = 0; $k < count($excelData[$j]); $k++) { //读取每行的数据进行写入
                $position = $excelTag[$k] . $tmp1;
                $body->setCellValueExplicit($position, $excelData[$j][$k], PHPExcel_Cell_DataType::TYPE_STRING);
            }
        }
        // 设置表名
        $spreadsheet->getActiveSheet()->setTitle($sheetTitle);

        // Set active sheet index to the first sheet, so Excel opens this as the first sheet
        $spreadsheet->setActiveSheetIndex(0);

        ob_end_clean();

        ob_start();

        // Redirect output to a client’s web browser (Xlsx)
        header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
        header('Content-Disposition: attachment;filename="' . $fileName . '.xlsx"');
        header('Cache-Control: max-age=0');
        // If you're serving to IE 9, then the following may be needed
        header('Cache-Control: max-age=1');

        // If you're serving to IE over SSL, then the following may be needed
        header('Expires: Mon, 26 Jul 1997 05:00:00 GMT'); // Date in the past
        header('Last-Modified: ' . gmdate('D, d M Y H:i:s') . ' GMT'); // always modified
        header('Cache-Control: cache, must-revalidate'); // HTTP/1.1
        header('Pragma: public'); // HTTP/1.0

        $writer = IOFactory::createWriter($spreadsheet, 'Xlsx');
        $writer->save('php://output');
        exit;
    }


    public static function getDealRecordSheetData($file_path)
    {
        $arr = [];
        if (!file_exists($file_path)) {
            $arr['result']  = false;
            $arr['message'] = "当前文件不存在。无法获取";
            $arr['data']    = '';

            return $arr;
        }
        //创建文件读取类对象
        $reader = IOFactory::createReader("Xlsx");
        /**  Advise the Reader that we only want to load cell data  **/
        //设置为只读模式$reader->setReadDataOnly(true);
        /**  Load $inputFileName to a Spreadsheet Object  **/
        //加载文件路径，路径为绝对路径
        $spreadsheet = $reader->load($file_path);

        //获取excel表中有多少个sheet
        $sheetNum  = $spreadsheet->getSheetCount();
        $sheetData = [];
        //只获取活跃的sheet
        //$temp = $spreadsheet->getSheetActive()->toArray(null, true, true, true);
        //遍历每一个sheet，获取里面的数据内容
        for ($i = 0; $i < $sheetNum; $i++) {
            $temp = $spreadsheet->getSheet($i)->toArray(null, true, true, true);

            foreach ($temp as $row) {
                $sheetData[] = $row;
            }
            $sheetData = array_slice($sheetData, 1, sizeof($sheetData));
        }

        return $sheetData;
    }
}

?>