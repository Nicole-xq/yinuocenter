<?php

namespace api\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use api\models\ShopncMember;

/**
 * ShopncMemberSearch represents the model behind the search form of `api\models\ShopncMember`.
 */
class ShopncMemberSearch extends ShopncMember
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['member_id', 'member_sex', 'member_email_bind', 'member_mobile_bind', 'member_login_num', 'member_points', 'inform_allow', 'is_buy', 'is_allowtalk', 'member_state', 'member_snsvisitnum', 'member_areaid', 'member_cityid', 'member_provinceid', 'member_exppoints', 'distri_state', 'distri_time', 'distri_handle_time', 'distri_show', 'quit_time', 'distri_apply_times', 'distri_quit_times', 'member_alipay_bind', 'member_type', 'member_role', 'freeze_points', 'bind_member_id', 'bind_time', 'is_artist', 'register_award', 'source', 'distribute_lv_1', 'distribute_lv_2', 'recommended', 'is_operator', 'source_staff_id'], 'integer'],
            [['member_key', 'member_name', 'member_truename', 'member_avatar', 'member_real_name', 'member_using_mobile', 'member_profile', 'member_birthday', 'member_passwd', 'member_paypwd', 'member_email', 'member_mobile', 'member_qq', 'member_ww', 'member_time', 'member_login_time', 'member_old_login_time', 'member_login_ip', 'member_old_login_ip', 'member_qqopenid', 'member_qqinfo', 'member_sinaopenid', 'member_sinainfo', 'weixin_unionid', 'weixin_info', 'weixin_open_id', 'member_areainfo', 'member_privacy', 'auth_message', 'bill_user_name', 'bill_type_code', 'bill_type_number', 'bill_bank_name', 'distri_code', 'alipay_account', 'alipay_name', 'bind_member_name', 'registered_source'], 'safe'],
            [['available_predeposit', 'freeze_predeposit', 'available_rc_balance', 'freeze_rc_balance', 'trad_amount', 'freeze_trad', 'available_margin', 'default_amount', 'freeze_margin', 'available_commis', 'freeze_commis', 'td_amount'], 'number'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = ShopncMember::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'member_id' => $this->member_id,
            'member_sex' => $this->member_sex,
            'member_birthday' => $this->member_birthday,
            'member_email_bind' => $this->member_email_bind,
            'member_mobile_bind' => $this->member_mobile_bind,
            'member_login_num' => $this->member_login_num,
            'member_points' => $this->member_points,
            'available_predeposit' => $this->available_predeposit,
            'freeze_predeposit' => $this->freeze_predeposit,
            'available_rc_balance' => $this->available_rc_balance,
            'freeze_rc_balance' => $this->freeze_rc_balance,
            'inform_allow' => $this->inform_allow,
            'is_buy' => $this->is_buy,
            'is_allowtalk' => $this->is_allowtalk,
            'member_state' => $this->member_state,
            'member_snsvisitnum' => $this->member_snsvisitnum,
            'member_areaid' => $this->member_areaid,
            'member_cityid' => $this->member_cityid,
            'member_provinceid' => $this->member_provinceid,
            'member_exppoints' => $this->member_exppoints,
            'trad_amount' => $this->trad_amount,
            'distri_state' => $this->distri_state,
            'freeze_trad' => $this->freeze_trad,
            'distri_time' => $this->distri_time,
            'distri_handle_time' => $this->distri_handle_time,
            'distri_show' => $this->distri_show,
            'quit_time' => $this->quit_time,
            'distri_apply_times' => $this->distri_apply_times,
            'distri_quit_times' => $this->distri_quit_times,
            'member_alipay_bind' => $this->member_alipay_bind,
            'available_margin' => $this->available_margin,
            'default_amount' => $this->default_amount,
            'freeze_margin' => $this->freeze_margin,
            'member_type' => $this->member_type,
            'member_role' => $this->member_role,
            'freeze_points' => $this->freeze_points,
            'bind_member_id' => $this->bind_member_id,
            'bind_time' => $this->bind_time,
            'available_commis' => $this->available_commis,
            'freeze_commis' => $this->freeze_commis,
            'is_artist' => $this->is_artist,
            'td_amount' => $this->td_amount,
            'register_award' => $this->register_award,
            'source' => $this->source,
            'distribute_lv_1' => $this->distribute_lv_1,
            'distribute_lv_2' => $this->distribute_lv_2,
            'recommended' => $this->recommended,
            'is_operator' => $this->is_operator,
            'source_staff_id' => $this->source_staff_id,
        ]);

        $query->andFilterWhere(['like', 'member_key', $this->member_key])
            ->andFilterWhere(['like', 'member_name', $this->member_name])
            ->andFilterWhere(['like', 'member_truename', $this->member_truename])
            ->andFilterWhere(['like', 'member_avatar', $this->member_avatar])
            ->andFilterWhere(['like', 'member_real_name', $this->member_real_name])
            ->andFilterWhere(['like', 'member_using_mobile', $this->member_using_mobile])
            ->andFilterWhere(['like', 'member_profile', $this->member_profile])
            ->andFilterWhere(['like', 'member_passwd', $this->member_passwd])
            ->andFilterWhere(['like', 'member_paypwd', $this->member_paypwd])
            ->andFilterWhere(['like', 'member_email', $this->member_email])
            ->andFilterWhere(['like', 'member_mobile', $this->member_mobile])
            ->andFilterWhere(['like', 'member_qq', $this->member_qq])
            ->andFilterWhere(['like', 'member_ww', $this->member_ww])
            ->andFilterWhere(['like', 'member_time', $this->member_time])
            ->andFilterWhere(['like', 'member_login_time', $this->member_login_time])
            ->andFilterWhere(['like', 'member_old_login_time', $this->member_old_login_time])
            ->andFilterWhere(['like', 'member_login_ip', $this->member_login_ip])
            ->andFilterWhere(['like', 'member_old_login_ip', $this->member_old_login_ip])
            ->andFilterWhere(['like', 'member_qqopenid', $this->member_qqopenid])
            ->andFilterWhere(['like', 'member_qqinfo', $this->member_qqinfo])
            ->andFilterWhere(['like', 'member_sinaopenid', $this->member_sinaopenid])
            ->andFilterWhere(['like', 'member_sinainfo', $this->member_sinainfo])
            ->andFilterWhere(['like', 'weixin_unionid', $this->weixin_unionid])
            ->andFilterWhere(['like', 'weixin_info', $this->weixin_info])
            ->andFilterWhere(['like', 'weixin_open_id', $this->weixin_open_id])
            ->andFilterWhere(['like', 'member_areainfo', $this->member_areainfo])
            ->andFilterWhere(['like', 'member_privacy', $this->member_privacy])
            ->andFilterWhere(['like', 'auth_message', $this->auth_message])
            ->andFilterWhere(['like', 'bill_user_name', $this->bill_user_name])
            ->andFilterWhere(['like', 'bill_type_code', $this->bill_type_code])
            ->andFilterWhere(['like', 'bill_type_number', $this->bill_type_number])
            ->andFilterWhere(['like', 'bill_bank_name', $this->bill_bank_name])
            ->andFilterWhere(['like', 'distri_code', $this->distri_code])
            ->andFilterWhere(['like', 'alipay_account', $this->alipay_account])
            ->andFilterWhere(['like', 'alipay_name', $this->alipay_name])
            ->andFilterWhere(['like', 'bind_member_name', $this->bind_member_name])
            ->andFilterWhere(['like', 'registered_source', $this->registered_source]);

        return $dataProvider;
    }
}
