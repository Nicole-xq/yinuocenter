<?php

namespace api\models;

use Yii;

/**
 * This is the model class for table "shopnc_member_commission".
 *
 * @property int $log_id 记录编号
 * @property int $order_id 订单编号
 * @property int $order_goods_id 订单商品编号
 * @property int $goods_id 商品编号
 * @property string $goods_name 商品名称
 * @property string $goods_image 订单商品图片
 * @property string $goods_pay_amount 商品支付金额
 * @property int $from_member_id 佣金来源用户ID
 * @property string $commission_amount 返佣金额
 * @property double $dis_commis_rate 返佣比例
 * @property int $dis_member_id 返佣佣金收益人
 * @property int $add_time 添加时间
 * @property int $dis_commis_state 佣金结算状态 0未结 1已结
 * @property string $goods_commission 订单商品佣金
 * @property int $commission_type 返佣来源类型:1商品订单2拍卖3下级保证金返佣4合伙人升级5艺术家入驻6保证金收益利息7拍卖竞价返佣8拍卖成交返佣9保证金利息返佣
 * @property string $pay_refund 退款金额
 * @property string $commission_refund 返佣退还佣金
 * @property string $goods_commission_refund 商品退还佣金
 * @property int $commission_time 返佣结算时间
 * @property string $from_member_name 返佣来源用户名
 * @property string $dis_member_name 返佣收益人用户名
 * @property string $order_sn 订单编号
 * @property int $top_lv 上级级别(1:上级,2:上级的上级..对应三级分销)
 * @property string $source_staff_id 来源员工ID
 * @property string $opreate_commission 竞价返给运营商金额,竞价幅度5%
 */
class ShopncMemberCommission extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'shopnc_member_commission';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['order_id', 'order_goods_id', 'goods_id', 'from_member_id', 'dis_member_id', 'add_time', 'dis_commis_state', 'commission_type', 'commission_time', 'top_lv', 'source_staff_id'], 'integer'],
            [['goods_pay_amount', 'commission_amount', 'dis_commis_rate', 'goods_commission', 'pay_refund', 'commission_refund', 'goods_commission_refund', 'opreate_commission'], 'number'],
            [['goods_name', 'goods_image', 'from_member_name', 'dis_member_name'], 'string', 'max' => 255],
            [['order_sn'], 'string', 'max' => 50],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'log_id' => '记录编号',
            'order_id' => '订单编号',
            'order_goods_id' => '订单商品编号',
            'goods_id' => '商品编号',
            'goods_name' => '商品名称',
            'goods_image' => '订单商品图片',
            'goods_pay_amount' => '商品支付金额',
            'from_member_id' => '佣金来源用户ID',
            'commission_amount' => '返佣金额',
            'dis_commis_rate' => '返佣比例',
            'dis_member_id' => '返佣佣金收益人',
            'add_time' => '添加时间',
            'dis_commis_state' => '佣金结算状态 0未结 1已结',
            'goods_commission' => '订单商品佣金',
            'commission_type' => '返佣来源类型:1商品订单2拍卖3下级保证金返佣4合伙人升级5艺术家入驻6保证金收益利息7拍卖竞价返佣8拍卖成交返佣9保证金利息返佣',
            'pay_refund' => '退款金额',
            'commission_refund' => '返佣退还佣金',
            'goods_commission_refund' => '商品退还佣金',
            'commission_time' => '返佣结算时间',
            'from_member_name' => '返佣来源用户名',
            'dis_member_name' => '返佣收益人用户名',
            'order_sn' => '订单编号',
            'top_lv' => '上级级别(1:上级,2:上级的上级..对应三级分销)',
            'source_staff_id' => '来源员工ID',
            'opreate_commission' => '竞价返给运营商金额,竞价幅度5%',
        ];
    }
}
