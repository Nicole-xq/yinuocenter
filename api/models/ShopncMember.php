<?php

namespace api\models;

use Yii;

/**
 * This is the model class for table "shopnc_member".
 *
 * @property int $member_id 会员id
 * @property string $member_key 用户唯一标识key
 * @property string $member_name 会员名称
 * @property string $member_truename 会员名称
 * @property string $member_avatar 会员头像
 * @property int $member_sex 会员性别
 * @property string $member_real_name 真实姓名
 * @property string $member_using_mobile 正在使用的手机
 * @property string $member_profile 会员简介
 * @property string $member_birthday 生日
 * @property string $member_passwd 会员密码
 * @property string $member_paypwd 支付密码
 * @property string $member_email 会员邮箱
 * @property int $member_email_bind 0未绑定1已绑定
 * @property string $member_mobile 手机号
 * @property int $member_mobile_bind 0未绑定1已绑定
 * @property string $member_qq qq
 * @property string $member_ww 阿里旺旺
 * @property int $member_login_num 登录次数
 * @property string $member_time 会员注册时间
 * @property string $member_login_time 当前登录时间
 * @property string $member_old_login_time 上次登录时间
 * @property string $member_login_ip 当前登录ip
 * @property string $member_old_login_ip 上次登录ip
 * @property string $member_qqopenid qq互联id
 * @property string $member_qqinfo qq账号相关信息
 * @property string $member_sinaopenid 新浪微博登录id
 * @property string $member_sinainfo 新浪账号相关信息序列化值
 * @property string $weixin_unionid 微信用户统一标识
 * @property string $weixin_info 微信用户相关信息
 * @property string $weixin_open_id 微信openID
 * @property int $member_points 会员积分
 * @property string $available_predeposit 预存款可用金额
 * @property string $freeze_predeposit 预存款冻结金额
 * @property string $available_rc_balance 可用充值卡余额
 * @property string $freeze_rc_balance 冻结充值卡余额
 * @property int $inform_allow 是否允许举报(1可以/2不可以)
 * @property int $is_buy 会员是否有购买权限 1为开启 0为关闭
 * @property int $is_allowtalk 会员是否有咨询和发送站内信的权限 1为开启 0为关闭
 * @property int $member_state 会员的开启状态 1为开启 0为关闭
 * @property int $member_snsvisitnum sns空间访问次数
 * @property int $member_areaid 地区ID
 * @property int $member_cityid 城市ID
 * @property int $member_provinceid 省份ID
 * @property string $member_areainfo 地区内容
 * @property string $member_privacy 隐私设定
 * @property int $member_exppoints 会员经验值
 * @property string $trad_amount 佣金总额
 * @property string $auth_message 审核意见
 * @property int $distri_state 分销状态 0未申请 1待审核 2已通过 3未通过 4清退 5退出
 * @property string $bill_user_name 收款人姓名
 * @property string $bill_type_code 结算账户类型
 * @property string $bill_type_number 收款账号
 * @property string $bill_bank_name 开户行
 * @property string $freeze_trad 冻结佣金
 * @property string $distri_code 分销代码
 * @property int $distri_time 申请时间
 * @property int $distri_handle_time 处理时间
 * @property int $distri_show 分销中心是否显示 0不显示 1显示
 * @property int $quit_time 退出时间
 * @property int $distri_apply_times 申请次数
 * @property int $distri_quit_times 退出次数
 * @property string $alipay_account 支付宝账户
 * @property string $alipay_name 支付宝名称
 * @property int $member_alipay_bind 支付宝绑定 0否 1是
 * @property string $available_margin 保证金账户
 * @property string $default_amount 违约金金额
 * @property string $freeze_margin 冻结保证金金额
 * @property int $member_type 会员类型 0注册会员 1普通会员 2特一级会员 3特二级会员 4艺术家
 * @property int $member_role 会员角色：0 注册会员，1 内部会员，2 导入会员
 * @property string $freeze_points 冻结积分
 * @property int $bind_member_id 绑定会员编号
 * @property string $bind_member_name 绑定会员名称
 * @property int $bind_time 绑定时间
 * @property string $available_commis 已返佣金
 * @property string $freeze_commis 待返佣金
 * @property int $is_artist 是否是艺术家 0:否   1:是
 * @property string $td_amount 购买特定专区商品金额
 * @property int $register_award 是否领取过注册奖励
 * @property int $source 用户来源  1,张雄艺术
 * @property int $distribute_lv_1 一级合伙人
 * @property int $distribute_lv_2 二级合伙人
 * @property int $recommended 是否已介绍拍乐赚专场0:未介绍,1:已介绍
 * @property string $registered_source 注册来源
 * @property int $is_operator 是否运营商0:非运营商,1:运营商
 * @property int $source_staff_id 来源员工ID
 */
class ShopncMember extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'shopnc_member';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['member_name', 'member_passwd', 'member_email', 'member_time', 'member_login_time', 'member_old_login_time'], 'required'],
            [['member_sex', 'member_email_bind', 'member_mobile_bind', 'member_login_num', 'member_points', 'inform_allow', 'is_buy', 'is_allowtalk', 'member_state', 'member_snsvisitnum', 'member_areaid', 'member_cityid', 'member_provinceid', 'member_exppoints', 'distri_state', 'distri_time', 'distri_handle_time', 'distri_show', 'quit_time', 'distri_apply_times', 'distri_quit_times', 'member_alipay_bind', 'member_type', 'member_role', 'freeze_points', 'bind_member_id', 'bind_time', 'is_artist', 'register_award', 'source', 'distribute_lv_1', 'distribute_lv_2', 'recommended', 'is_operator', 'source_staff_id'], 'integer'],
            [['member_birthday'], 'safe'],
            [['member_qqinfo', 'member_sinainfo', 'weixin_info', 'member_privacy'], 'string'],
            [['available_predeposit', 'freeze_predeposit', 'available_rc_balance', 'freeze_rc_balance', 'trad_amount', 'freeze_trad', 'available_margin', 'default_amount', 'freeze_margin', 'available_commis', 'freeze_commis', 'td_amount'], 'number'],
            [['member_key', 'member_areainfo', 'auth_message', 'bill_user_name', 'bill_type_code', 'bill_type_number', 'bill_bank_name', 'distri_code', 'alipay_account', 'alipay_name', 'bind_member_name'], 'string', 'max' => 255],
            [['member_name', 'member_real_name', 'weixin_unionid', 'weixin_open_id', 'registered_source'], 'string', 'max' => 50],
            [['member_truename'], 'string', 'max' => 30],
            [['member_avatar'], 'string', 'max' => 200],
            [['member_using_mobile', 'member_mobile'], 'string', 'max' => 11],
            [['member_profile'], 'string', 'max' => 300],
            [['member_passwd', 'member_paypwd'], 'string', 'max' => 32],
            [['member_email', 'member_qq', 'member_ww', 'member_qqopenid', 'member_sinaopenid'], 'string', 'max' => 100],
            [['member_time', 'member_login_time', 'member_old_login_time'], 'string', 'max' => 10],
            [['member_login_ip', 'member_old_login_ip'], 'string', 'max' => 20],
            [['member_name'], 'unique'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'member_id' => '会员id',
            'member_key' => '用户唯一标识key',
            'member_name' => '会员名称',
            'member_truename' => '会员名称',
            'member_avatar' => '会员头像',
            'member_sex' => '会员性别',
            'member_real_name' => '真实姓名',
            'member_using_mobile' => '正在使用的手机',
            'member_profile' => '会员简介',
            'member_birthday' => '生日',
            'member_passwd' => '会员密码',
            'member_paypwd' => '支付密码',
            'member_email' => '会员邮箱',
            'member_email_bind' => '0未绑定1已绑定',
            'member_mobile' => '手机号',
            'member_mobile_bind' => '0未绑定1已绑定',
            'member_qq' => 'qq',
            'member_ww' => '阿里旺旺',
            'member_login_num' => '登录次数',
            'member_time' => '会员注册时间',
            'member_login_time' => '当前登录时间',
            'member_old_login_time' => '上次登录时间',
            'member_login_ip' => '当前登录ip',
            'member_old_login_ip' => '上次登录ip',
            'member_qqopenid' => 'qq互联id',
            'member_qqinfo' => 'qq账号相关信息',
            'member_sinaopenid' => '新浪微博登录id',
            'member_sinainfo' => '新浪账号相关信息序列化值',
            'weixin_unionid' => '微信用户统一标识',
            'weixin_info' => '微信用户相关信息',
            'weixin_open_id' => '微信openID',
            'member_points' => '会员积分',
            'available_predeposit' => '预存款可用金额',
            'freeze_predeposit' => '预存款冻结金额',
            'available_rc_balance' => '可用充值卡余额',
            'freeze_rc_balance' => '冻结充值卡余额',
            'inform_allow' => '是否允许举报(1可以/2不可以)',
            'is_buy' => '会员是否有购买权限 1为开启 0为关闭',
            'is_allowtalk' => '会员是否有咨询和发送站内信的权限 1为开启 0为关闭',
            'member_state' => '会员的开启状态 1为开启 0为关闭',
            'member_snsvisitnum' => 'sns空间访问次数',
            'member_areaid' => '地区ID',
            'member_cityid' => '城市ID',
            'member_provinceid' => '省份ID',
            'member_areainfo' => '地区内容',
            'member_privacy' => '隐私设定',
            'member_exppoints' => '会员经验值',
            'trad_amount' => '佣金总额',
            'auth_message' => '审核意见',
            'distri_state' => '分销状态 0未申请 1待审核 2已通过 3未通过 4清退 5退出',
            'bill_user_name' => '收款人姓名',
            'bill_type_code' => '结算账户类型',
            'bill_type_number' => '收款账号',
            'bill_bank_name' => '开户行',
            'freeze_trad' => '冻结佣金',
            'distri_code' => '分销代码',
            'distri_time' => '申请时间',
            'distri_handle_time' => '处理时间',
            'distri_show' => '分销中心是否显示 0不显示 1显示',
            'quit_time' => '退出时间',
            'distri_apply_times' => '申请次数',
            'distri_quit_times' => '退出次数',
            'alipay_account' => '支付宝账户',
            'alipay_name' => '支付宝名称',
            'member_alipay_bind' => '支付宝绑定 0否 1是',
            'available_margin' => '保证金账户',
            'default_amount' => '违约金金额',
            'freeze_margin' => '冻结保证金金额',
            'member_type' => '会员类型 0注册会员 1普通会员 2特一级会员 3特二级会员 4艺术家',
            'member_role' => '会员角色：0 注册会员，1 内部会员，2 导入会员',
            'freeze_points' => '冻结积分',
            'bind_member_id' => '绑定会员编号',
            'bind_member_name' => '绑定会员名称',
            'bind_time' => '绑定时间',
            'available_commis' => '已返佣金',
            'freeze_commis' => '待返佣金',
            'is_artist' => '是否是艺术家 0:否   1:是',
            'td_amount' => '购买特定专区商品金额',
            'register_award' => '是否领取过注册奖励',
            'source' => '用户来源  1,张雄艺术',
            'distribute_lv_1' => '一级合伙人',
            'distribute_lv_2' => '二级合伙人',
            'recommended' => '是否已介绍拍乐赚专场0:未介绍,1:已介绍',
            'registered_source' => '注册来源',
            'is_operator' => '是否运营商0:非运营商,1:运营商',
            'source_staff_id' => '来源员工ID',
        ];
    }

    public function getSourceStaff($member_id)
    {
        return $this->find()->where(['member_id' => $member_id])->asArray()->one();
    }

}
