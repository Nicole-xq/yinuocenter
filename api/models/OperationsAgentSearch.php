<?php

namespace api\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use api\models\OperationsAgent;

/**
 * OperationsAgentSearch represents the model behind the search form of `api\models\OperationsAgent`.
 */
class OperationsAgentSearch extends OperationsAgent
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['agent_id', 'source_id', 'agent_type', 'agent_status'], 'integer'],
            [['agent_name', 'agent_phone', 'agent_passwd', 'created_at'], 'safe'],
            [['commission_rate'], 'number'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = OperationsAgent::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'agent_id' => $this->agent_id,
            'source_id' => $this->source_id,
            'commission_rate' => $this->commission_rate,
            'agent_type' => $this->agent_type,
            'created_at' => $this->created_at,
            'agent_status' => $this->agent_status,
        ]);

        $query->andFilterWhere(['like', 'agent_name', $this->agent_name])
            ->andFilterWhere(['like', 'agent_phone', $this->agent_phone])
            ->andFilterWhere(['like', 'agent_passwd', $this->agent_passwd]);

        return $dataProvider;
    }
}
