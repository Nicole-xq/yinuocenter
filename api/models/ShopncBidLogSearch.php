<?php

namespace api\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use api\models\ShopncBidLog;

/**
 * ShopncBidLogSearch represents the model behind the search form of `api\models\ShopncBidLog`.
 */
class ShopncBidLogSearch extends ShopncBidLog
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['bid_id', 'member_id', 'auction_id', 'created_at', 'is_anonymous', 'bid_type'], 'integer'],
            [['offer_num', 'commission_amount'], 'number'],
            [['member_name', 'member_avatar'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = ShopncBidLog::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'bid_id' => $this->bid_id,
            'member_id' => $this->member_id,
            'auction_id' => $this->auction_id,
            'created_at' => $this->created_at,
            'offer_num' => $this->offer_num,
            'is_anonymous' => $this->is_anonymous,
            'commission_amount' => $this->commission_amount,
            'bid_type' => $this->bid_type,
        ]);

        $query->andFilterWhere(['like', 'member_name', $this->member_name])
            ->andFilterWhere(['like', 'member_avatar', $this->member_avatar]);

        return $dataProvider;
    }
}
