<?php

namespace api\models;

use Yii;

/**
 * This is the model class for table "shopnc_member_distribute_type".
 *
 * @property int $id 主键id
 * @property int $type_id 分销类型id
 * @property string $name 名字
 * @property string $logo_img logo
 * @property string $price 特定专区升级金额
 * @property string $wdl_num 微代理分销百分比
 * @property string $zl_num 战略合伙人百分比
 * @property string $sy_num 事业合伙人百分比
 * @property string $artist_num 艺术家百分比
 * @property string $goods_num 购买艺术品百分比
 * @property string $bzj_num 拍卖保证金百分比
 * @property int $create_time 创建时间
 * @property int $last_modify_time 最后修改时间
 */
class ShopncMemberDistributeType extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'shopnc_member_distribute_type';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'type_id', 'name'], 'required'],
            [['id', 'type_id', 'create_time', 'last_modify_time'], 'integer'],
            [['price', 'wdl_num', 'zl_num', 'sy_num', 'artist_num', 'goods_num', 'bzj_num'], 'number'],
            [['name'], 'string', 'max' => 20],
            [['logo_img'], 'string', 'max' => 255],
            [['id'], 'unique'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => '主键id',
            'type_id' => '分销类型id',
            'name' => '名字',
            'logo_img' => 'logo',
            'price' => '特定专区升级金额',
            'wdl_num' => '微代理分销百分比',
            'zl_num' => '战略合伙人百分比',
            'sy_num' => '事业合伙人百分比',
            'artist_num' => '艺术家百分比',
            'goods_num' => '购买艺术品百分比',
            'bzj_num' => '拍卖保证金百分比',
            'create_time' => '创建时间',
            'last_modify_time' => '最后修改时间',
        ];
    }
}
