<?php

namespace api\models;

use Yii;

/**
 * This is the model class for table "shopnc_orders".
 *
 * @property int $order_id 订单索引id
 * @property string $order_sn 订单编号
 * @property string $pay_sn 支付单号
 * @property string $pay_sn1 预定订单支付订金时的支付单号
 * @property string $store_id 卖家店铺id
 * @property string $store_name 卖家店铺名称
 * @property string $buyer_id 买家id
 * @property string $seller_id 卖家ID
 * @property string $buyer_name 买家姓名
 * @property string $buyer_email 买家电子邮箱
 * @property string $buyer_phone 买家手机
 * @property string $add_time 订单生成时间
 * @property string $payment_code 支付方式名称代码
 * @property string $payment_time 支付(付款)时间
 * @property string $finnshed_time 订单完成时间
 * @property string $goods_amount 商品总价格
 * @property string $order_amount 订单总价格(不包括红包,包括诺币的实价)
 * @property string $rcb_amount 充值卡支付金额
 * @property string $pd_amount 预存款支付金额
 * @property string $shipping_fee 运费
 * @property int $evaluation_state 评价状态 0未评价，1已评价，2已过期未评价
 * @property int $evaluation_again_state 追加评价状态 0未评价，1已评价，2已过期未评价
 * @property int $order_state 订单状态：0(已取消)10(默认):未付款;20:已付款;30:已发货;40:已收货;
 * @property int $refund_state 退款状态:0是无退款,1是部分退款,2是全部退款
 * @property int $lock_state 锁定状态:0是正常,大于0是锁定,默认是0
 * @property int $delete_state 删除状态0未删除1放入回收站2彻底删除
 * @property string $refund_amount 退款金额
 * @property string $delay_time 延迟时间,默认为0
 * @property int $order_from 1WEB2mobile
 * @property string $shipping_code 物流单号
 * @property int $order_type 订单类型1普通订单(默认),2预定订单,3门店自提订单,4拍卖订单,5拍乐赚订单,6捡漏订单,7推广订单
 * @property string $api_pay_time 在线支付动作时间,只要向第三方支付平台提交就会更新
 * @property string $api_pay_amount 第三方支付金额
 * @property string $chain_id 自提门店ID
 * @property string $chain_code 门店提货码
 * @property string $rpt_amount 红包值
 * @property string $trade_no 外部交易订单号
 * @property int $is_dis 是否分销订单
 * @property string $pay_voucher 订单的付款凭证
 * @property string $is_points 是否使用积分 0否 1是
 * @property string $points_amount 积分抵扣现金
 * @property string $margin_amount 保证金抵扣
 * @property int $gs_id 趣猜id
 * @property int $commission_state 返佣状态
 * @property int $auction_id 拍卖id
 * @property int $notice_state 消息提醒：0未提醒，1已经提醒
 * @property int $artist_closing_status 艺术家结算状态 0.默认状态未结算  10.已结算
 * @property string $artist_closing_time 艺术家结算时间
 * @property string $order_amount_original 订单原始总价格
 * @property string $artist_closing_no 艺术家结算编号
 * @property string $artist_closing_amount 艺术家结算金额
 * @property string $artist_id 艺术家id
 * @property string $auction_cost 拍品成本价格
 * @property string $top_member_id 上级推荐人ID
 * @property string $source_staff_id 来源员工ID
 * @property string $commission_amount 订单总奖励金(成交返佣加竞价返佣)
 */
class ShopncOrders extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'shopnc_orders';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['order_sn', 'pay_sn', 'store_id', 'store_name', 'buyer_id', 'buyer_name'], 'required'],
            [['order_sn', 'pay_sn', 'pay_sn1', 'store_id', 'buyer_id', 'seller_id', 'buyer_phone', 'add_time', 'payment_time', 'finnshed_time', 'evaluation_state', 'evaluation_again_state', 'order_state', 'refund_state', 'lock_state', 'delete_state', 'delay_time', 'order_from', 'order_type', 'api_pay_time', 'chain_id', 'chain_code', 'is_dis', 'is_points', 'gs_id', 'commission_state', 'auction_id', 'notice_state', 'artist_closing_status', 'artist_id', 'top_member_id', 'source_staff_id'], 'integer'],
            [['goods_amount', 'order_amount', 'rcb_amount', 'pd_amount', 'shipping_fee', 'refund_amount', 'api_pay_amount', 'rpt_amount', 'points_amount', 'margin_amount', 'order_amount_original', 'artist_closing_amount', 'auction_cost', 'commission_amount'], 'number'],
            [['artist_closing_time'], 'safe'],
            [['store_name', 'buyer_name', 'shipping_code', 'trade_no'], 'string', 'max' => 50],
            [['buyer_email'], 'string', 'max' => 80],
            [['payment_code'], 'string', 'max' => 10],
            [['pay_voucher'], 'string', 'max' => 255],
            [['artist_closing_no'], 'string', 'max' => 30],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'order_id' => '订单索引id',
            'order_sn' => '订单编号',
            'pay_sn' => '支付单号',
            'pay_sn1' => '预定订单支付订金时的支付单号',
            'store_id' => '卖家店铺id',
            'store_name' => '卖家店铺名称',
            'buyer_id' => '买家id',
            'seller_id' => '卖家ID',
            'buyer_name' => '买家姓名',
            'buyer_email' => '买家电子邮箱',
            'buyer_phone' => '买家手机',
            'add_time' => '订单生成时间',
            'payment_code' => '支付方式名称代码',
            'payment_time' => '支付(付款)时间',
            'finnshed_time' => '订单完成时间',
            'goods_amount' => '商品总价格',
            'order_amount' => '订单总价格(不包括红包,包括诺币的实价)',
            'rcb_amount' => '充值卡支付金额',
            'pd_amount' => '预存款支付金额',
            'shipping_fee' => '运费',
            'evaluation_state' => '评价状态 0未评价，1已评价，2已过期未评价',
            'evaluation_again_state' => '追加评价状态 0未评价，1已评价，2已过期未评价',
            'order_state' => '订单状态：0(已取消)10(默认):未付款;20:已付款;30:已发货;40:已收货;',
            'refund_state' => '退款状态:0是无退款,1是部分退款,2是全部退款',
            'lock_state' => '锁定状态:0是正常,大于0是锁定,默认是0',
            'delete_state' => '删除状态0未删除1放入回收站2彻底删除',
            'refund_amount' => '退款金额',
            'delay_time' => '延迟时间,默认为0',
            'order_from' => '1WEB2mobile',
            'shipping_code' => '物流单号',
            'order_type' => '订单类型1普通订单(默认),2预定订单,3门店自提订单,4拍卖订单,5拍乐赚订单,6捡漏订单,7推广订单',
            'api_pay_time' => '在线支付动作时间,只要向第三方支付平台提交就会更新',
            'api_pay_amount' => '第三方支付金额',
            'chain_id' => '自提门店ID',
            'chain_code' => '门店提货码',
            'rpt_amount' => '红包值',
            'trade_no' => '外部交易订单号',
            'is_dis' => '是否分销订单',
            'pay_voucher' => '订单的付款凭证',
            'is_points' => '是否使用积分 0否 1是',
            'points_amount' => '积分抵扣现金',
            'margin_amount' => '保证金抵扣',
            'gs_id' => '趣猜id',
            'commission_state' => '返佣状态',
            'auction_id' => '拍卖id',
            'notice_state' => '消息提醒：0未提醒，1已经提醒',
            'artist_closing_status' => '艺术家结算状态 0.默认状态未结算  10.已结算',
            'artist_closing_time' => '艺术家结算时间',
            'order_amount_original' => '订单原始总价格',
            'artist_closing_no' => '艺术家结算编号',
            'artist_closing_amount' => '艺术家结算金额',
            'artist_id' => '艺术家id',
            'auction_cost' => '拍品成本价格',
            'top_member_id' => '上级推荐人ID',
            'source_staff_id' => '来源员工ID',
            'commission_amount' => '订单总奖励金(成交返佣加竞价返佣)',
        ];
    }
}
