<?php

namespace api\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use api\models\ShopncOrders;

/**
 * ShopncOrdersSearch represents the model behind the search form of `api\models\ShopncOrders`.
 */
class ShopncOrdersSearch extends ShopncOrders
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['order_id', 'order_sn', 'pay_sn', 'pay_sn1', 'store_id', 'buyer_id', 'seller_id', 'buyer_phone', 'add_time', 'payment_time', 'finnshed_time', 'evaluation_state', 'evaluation_again_state', 'order_state', 'refund_state', 'lock_state', 'delete_state', 'delay_time', 'order_from', 'order_type', 'api_pay_time', 'chain_id', 'chain_code', 'is_dis', 'is_points', 'gs_id', 'commission_state', 'auction_id', 'notice_state', 'artist_closing_status', 'artist_id', 'top_member_id', 'source_staff_id'], 'integer'],
            [['store_name', 'buyer_name', 'buyer_email', 'payment_code', 'shipping_code', 'trade_no', 'pay_voucher', 'artist_closing_time', 'artist_closing_no'], 'safe'],
            [['goods_amount', 'order_amount', 'rcb_amount', 'pd_amount', 'shipping_fee', 'refund_amount', 'api_pay_amount', 'rpt_amount', 'points_amount', 'margin_amount', 'order_amount_original', 'artist_closing_amount', 'auction_cost', 'commission_amount'], 'number'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = ShopncOrders::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'order_id' => $this->order_id,
            'order_sn' => $this->order_sn,
            'pay_sn' => $this->pay_sn,
            'pay_sn1' => $this->pay_sn1,
            'store_id' => $this->store_id,
            'buyer_id' => $this->buyer_id,
            'seller_id' => $this->seller_id,
            'buyer_phone' => $this->buyer_phone,
            'add_time' => $this->add_time,
            'payment_time' => $this->payment_time,
            'finnshed_time' => $this->finnshed_time,
            'goods_amount' => $this->goods_amount,
            'order_amount' => $this->order_amount,
            'rcb_amount' => $this->rcb_amount,
            'pd_amount' => $this->pd_amount,
            'shipping_fee' => $this->shipping_fee,
            'evaluation_state' => $this->evaluation_state,
            'evaluation_again_state' => $this->evaluation_again_state,
            'order_state' => $this->order_state,
            'refund_state' => $this->refund_state,
            'lock_state' => $this->lock_state,
            'delete_state' => $this->delete_state,
            'refund_amount' => $this->refund_amount,
            'delay_time' => $this->delay_time,
            'order_from' => $this->order_from,
            'order_type' => $this->order_type,
            'api_pay_time' => $this->api_pay_time,
            'api_pay_amount' => $this->api_pay_amount,
            'chain_id' => $this->chain_id,
            'chain_code' => $this->chain_code,
            'rpt_amount' => $this->rpt_amount,
            'is_dis' => $this->is_dis,
            'is_points' => $this->is_points,
            'points_amount' => $this->points_amount,
            'margin_amount' => $this->margin_amount,
            'gs_id' => $this->gs_id,
            'commission_state' => $this->commission_state,
            'auction_id' => $this->auction_id,
            'notice_state' => $this->notice_state,
            'artist_closing_status' => $this->artist_closing_status,
            'artist_closing_time' => $this->artist_closing_time,
            'order_amount_original' => $this->order_amount_original,
            'artist_closing_amount' => $this->artist_closing_amount,
            'artist_id' => $this->artist_id,
            'auction_cost' => $this->auction_cost,
            'top_member_id' => $this->top_member_id,
            'source_staff_id' => $this->source_staff_id,
            'commission_amount' => $this->commission_amount,
        ]);

        $query->andFilterWhere(['like', 'store_name', $this->store_name])
            ->andFilterWhere(['like', 'buyer_name', $this->buyer_name])
            ->andFilterWhere(['like', 'buyer_email', $this->buyer_email])
            ->andFilterWhere(['like', 'payment_code', $this->payment_code])
            ->andFilterWhere(['like', 'shipping_code', $this->shipping_code])
            ->andFilterWhere(['like', 'trade_no', $this->trade_no])
            ->andFilterWhere(['like', 'pay_voucher', $this->pay_voucher])
            ->andFilterWhere(['like', 'artist_closing_no', $this->artist_closing_no]);

        return $dataProvider;
    }
}
