<?php

namespace api\models;

use Yii;

/**
 * This is the model class for table "shopnc_operations_agent".
 *
 * @property int $agent_id 代理ID
 * @property int $source_id 来源ID
 * @property string $agent_name 代理姓名
 * @property string $agent_phone 代理手机
 * @property double $commission_rate 返佣比例
 * @property int $agent_type 代理类别(1代理,2员工)
 * @property string $created_at 创建时间
 * @property int $agent_status 代理状态(1启用,2禁用)
 */
class OperationsAgent extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'shopnc_operations_agent';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['source_id', 'agent_name', 'agent_phone', 'commission_rate', 'agent_type', 'agent_status'], 'required'],
            [['source_id', 'agent_type', 'agent_status'], 'integer'],
            [['commission_rate'], 'number'],
            [['created_at'], 'safe'],
            [['agent_name'], 'string', 'max' => 64],
            [['agent_phone'], 'string', 'max' => 32],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'agent_id' => '代理ID',
            'source_id' => '来源ID',
            'agent_name' => '代理姓名',
            'agent_phone' => '代理手机',
            'commission_rate' => '返佣比例',
            'agent_type' => '代理类别(1代理,2员工)',
            'created_at' => '创建时间',
            'agent_status' => '代理状态(1启用,2禁用)',
        ];
    }
}
