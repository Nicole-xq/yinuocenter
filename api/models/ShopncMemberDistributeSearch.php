<?php

namespace api\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;

/**
 * ShopncMemberDistributeSearch represents the model behind the search form of `api\models\ShopncMemberDistribute`.
 */
class ShopncMemberDistributeSearch extends ShopncMemberDistribute
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['dis_id', 'member_id', 'add_time', 'top_member', 'top_member_2', 'member_type', 'qr_expire_seconds'], 'integer'],
            [['dis_code', 'member_mobile', 'member_name', 'member_comment', 'member_comment_2', 'top_member_name', 'wx_qrcode_ticket', 'wx_qrcode_url'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = ShopncMemberDistribute::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'dis_id' => $this->dis_id,
            'member_id' => $this->member_id,
            'add_time' => $this->add_time,
            'top_member' => $this->top_member,
            'top_member_2' => $this->top_member_2,
            'member_type' => $this->member_type,
            'qr_expire_seconds' => $this->qr_expire_seconds,
        ]);

        $query->andFilterWhere(['like', 'dis_code', $this->dis_code])
            ->andFilterWhere(['like', 'member_mobile', $this->member_mobile])
            ->andFilterWhere(['like', 'member_name', $this->member_name])
            ->andFilterWhere(['like', 'member_comment', $this->member_comment])
            ->andFilterWhere(['like', 'member_comment_2', $this->member_comment_2])
            ->andFilterWhere(['like', 'top_member_name', $this->top_member_name])
            ->andFilterWhere(['like', 'wx_qrcode_ticket', $this->wx_qrcode_ticket])
            ->andFilterWhere(['like', 'wx_qrcode_url', $this->wx_qrcode_url]);

        return $dataProvider;
    }

    //查询运营商所有下级用户,返回字符串
    public function getMemberIds($top_member)
    {
        $getMemberIds = ShopncMemberDistribute::find()->select('member_id')->where(['top_member'=>$top_member])->asArray()->all();
        $getMemberIds = implode(",", array_column($getMemberIds, 'member_id'));
        return $getMemberIds;
    }

    //查询运营商所有下级用户,返回数组
    public function getMemberIds2($top_member)
    {
        $getMemberIds = ShopncMemberDistribute::find()->select('member_id')->where(['top_member'=>$top_member])->asArray()->all();
        $getMemberIds = array_column($getMemberIds, 'member_id');
        return $getMemberIds;
    }
}
