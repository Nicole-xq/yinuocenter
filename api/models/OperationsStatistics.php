<?php

namespace api\models;

use Yii;

/**
 * This is the model class for table "shopnc_operations_statistics".
 *
 * @property int $id 总账ID
 * @property int $source_id 来源ID
 * @property string $order_sn 订单编号
 * @property string $commission_amount 返佣金额
 * @property int $amount_type 类型(1入账2出账)
 * @property int $amount_status 状态(1成功2失败)
 * @property string $amount_remarks 备注
 * @property string $created_at 创建时间
 * @property string $updated_at 更新时间
 */
class OperationsStatistics extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'shopnc_operations_statistics';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'source_id', 'order_sn', 'amount_type', 'amount_status'], 'required'],
            [['id', 'source_id', 'order_sn', 'amount_type', 'amount_status'], 'integer'],
            [['commission_amount'], 'number'],
            [['created_at', 'updated_at'], 'safe'],
            [['amount_remarks'], 'string', 'max' => 255],
            [['id'], 'unique'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => '总账ID',
            'source_id' => '来源ID',
            'order_sn' => '订单编号',
            'commission_amount' => '返佣金额',
            'amount_type' => '类型(1入账2出账)',
            'amount_status' => '状态(1成功2失败)',
            'amount_remarks' => '备注',
            'created_at' => '创建时间',
            'updated_at' => '更新时间',
        ];
    }
}
