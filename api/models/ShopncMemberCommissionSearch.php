<?php

namespace api\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use api\models\ShopncMemberCommission;

/**
 * ShopncMemberCommissionSearch represents the model behind the search form of `api\models\ShopncMemberCommission`.
 */
class ShopncMemberCommissionSearch extends ShopncMemberCommission
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['log_id', 'order_id', 'order_goods_id', 'goods_id', 'from_member_id', 'dis_member_id', 'add_time', 'dis_commis_state', 'commission_type', 'commission_time', 'top_lv', 'source_staff_id'], 'integer'],
            [['goods_name', 'goods_image', 'from_member_name', 'dis_member_name', 'order_sn'], 'safe'],
            [['goods_pay_amount', 'commission_amount', 'dis_commis_rate', 'goods_commission', 'pay_refund', 'commission_refund', 'goods_commission_refund', 'opreate_commission'], 'number'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = ShopncMemberCommission::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'log_id' => $this->log_id,
            'order_id' => $this->order_id,
            'order_goods_id' => $this->order_goods_id,
            'goods_id' => $this->goods_id,
            'goods_pay_amount' => $this->goods_pay_amount,
            'from_member_id' => $this->from_member_id,
            'commission_amount' => $this->commission_amount,
            'dis_commis_rate' => $this->dis_commis_rate,
            'dis_member_id' => $this->dis_member_id,
            'add_time' => $this->add_time,
            'dis_commis_state' => $this->dis_commis_state,
            'goods_commission' => $this->goods_commission,
            'commission_type' => $this->commission_type,
            'pay_refund' => $this->pay_refund,
            'commission_refund' => $this->commission_refund,
            'goods_commission_refund' => $this->goods_commission_refund,
            'commission_time' => $this->commission_time,
            'top_lv' => $this->top_lv,
            'source_staff_id' => $this->source_staff_id,
            'opreate_commission' => $this->opreate_commission,
        ]);

        $query->andFilterWhere(['like', 'goods_name', $this->goods_name])
            ->andFilterWhere(['like', 'goods_image', $this->goods_image])
            ->andFilterWhere(['like', 'from_member_name', $this->from_member_name])
            ->andFilterWhere(['like', 'dis_member_name', $this->dis_member_name])
            ->andFilterWhere(['like', 'order_sn', $this->order_sn]);

        return $dataProvider;
    }
}
