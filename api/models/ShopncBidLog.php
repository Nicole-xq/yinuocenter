<?php

namespace api\models;

use Yii;

/**
 * This is the model class for table "shopnc_bid_log".
 *
 * @property string $bid_id 出价日志ID
 * @property int $member_id 会员ID
 * @property int $auction_id 拍品ID
 * @property int $created_at 创建时间
 * @property string $offer_num 出价价格
 * @property string $member_name 出价人会员名
 * @property int $is_anonymous 是否匿名，1：匿名，0：不匿名
 * @property string $commission_amount 用户竞价返佣金额
 * @property int $bid_type 1: 拍卖,2:新手体验拍品, 3:新手专区竞价
 * @property string $member_avatar 会员头像
 */
class ShopncBidLog extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'shopnc_bid_log';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['member_id', 'auction_id', 'created_at'], 'required'],
            [['member_id', 'auction_id', 'created_at', 'is_anonymous', 'bid_type'], 'integer'],
            [['offer_num', 'commission_amount'], 'number'],
            [['member_name'], 'string', 'max' => 255],
            [['member_avatar'], 'string', 'max' => 200],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'bid_id' => '出价日志ID',
            'member_id' => '会员ID',
            'auction_id' => '拍品ID',
            'created_at' => '创建时间',
            'offer_num' => '出价价格',
            'member_name' => '出价人会员名',
            'is_anonymous' => '是否匿名，1：匿名，0：不匿名',
            'commission_amount' => '用户竞价返佣金额',
            'bid_type' => '1: 拍卖,2:新手体验拍品, 3:新手专区竞价',
            'member_avatar' => '会员头像',
        ];
    }

    //获取竞价拍品信息
    public function getAuctions()
    {
        return $this->hasOne(ShopncAuctions::className(), ['auction_id'=>'auction_id']);
    }
}
