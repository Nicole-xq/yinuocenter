<?php

namespace api\models;

use Yii;

/**
 * This is the model class for table "shopnc_auctions".
 *
 * @property string $auction_id 拍卖品ID
 * @property int $goods_id 商品(SKU)id
 * @property int $goods_common_id 商品id
 * @property string $auction_name 拍品名称
 * @property int $auction_type 拍品类型: 0,普通拍品,1,新手拍品 2.新手专区 3.捡漏拍品
 * @property int $store_id 店铺ID
 * @property string $store_name 店铺名称
 * @property int $special_id 专场id
 * @property int $auction_add_time 拍品添加时间
 * @property int $auction_edit_time 拍品编辑时间
 * @property int $auction_preview_start 预展开始时间
 * @property int $auction_start_time 开始拍卖时间
 * @property int $auction_end_time 结束拍卖时间
 * @property int $auction_end_true_t 拍品真实结束时间
 * @property string $auction_image 拍品主图
 * @property string $auction_video 拍品视频
 * @property string $auction_bond 拍品保证金
 * @property string $auction_start_price 起拍价
 * @property string $auction_reference_price 参考价
 * @property string $auction_increase_range 加价幅度
 * @property string $auction_reserve_price 保留价
 * @property string $delivery_mechanism 送拍机构
 * @property int $state 拍卖状态: 0未结束 1拍卖结束
 * @property int $gc_id 拍卖分类
 * @property int $auction_lock 拍品锁定，0：未锁定，1：锁定
 * @property string $auction_click 拍品点击数量
 * @property int $is_liupai 是否流拍，0：未流拍，1：流拍
 * @property string $current_price 拍品当前价
 * @property string $bid_number 出价次数
 * @property string $num_of_applicants 报名人数
 * @property int $real_participants 真实参与人数
 * @property string $set_reminders_num 设置提醒人数
 * @property string $auction_collect 收藏量
 * @property int $recommended 推荐 1：推荐活动 0：普通活动
 * @property int $current_price_time 当前价出价时间
 * @property string $store_vendue_id 店铺拍卖表ID
 * @property string $auction_bond_rate 保证金利息
 * @property string $interest_last_date 最后计息日期
 * @property int $auctions_artist_id 艺术家ID
 * @property string $auctions_artist_name 艺术家名字
 * @property int $auctions_class_lv1 一级分类
 * @property int $auctions_class_lv2 二级分类
 * @property string $auctions_spec 规格json
 * @property int $auctions_long 长
 * @property int $auctions_width 宽
 * @property int $auctions_height 高
 * @property string $create_age 创作年代
 * @property string $auctions_summary 作品简介
 * @property string $other_images 明细图片
 * @property string $share_content 分享描述
 * @property int $duration_time 持续时间
 * @property string $warehouse_location 库位号,A12031:A区12号货架第31号库位
 * @property string $auction_cost 拍品成本价格
 * @property string $check_status 审核状态 10 未审核 20 审核成功 30审核失败
 * @property string $is_open 是否开启 0:关闭 1:开启
 * @property string $auction_sponsor 发起者 0:店铺 1:平台
 * @property string $is_recommend 是否推荐 0:不推荐 1:推荐
 * @property string $free_post 是否包邮 0:不包邮 1:包邮
 * @property string $express_fee 快递费
 * @property int $goods_storage 拍品库存
 * @property string $desc_content 作品简介
 */
class ShopncAuctions extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'shopnc_auctions';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['goods_id', 'goods_common_id', 'auction_type', 'store_id', 'special_id', 'auction_add_time', 'auction_edit_time', 'auction_preview_start', 'auction_start_time', 'auction_end_time', 'auction_end_true_t', 'state', 'gc_id', 'auction_lock', 'auction_click', 'is_liupai', 'bid_number', 'num_of_applicants', 'real_participants', 'set_reminders_num', 'auction_collect', 'recommended', 'current_price_time', 'store_vendue_id', 'auctions_artist_id', 'auctions_class_lv1', 'auctions_class_lv2', 'auctions_long', 'auctions_width', 'auctions_height', 'duration_time', 'check_status', 'is_open', 'auction_sponsor', 'is_recommend', 'free_post', 'goods_storage'], 'integer'],
            [['auction_name', 'store_id', 'store_name', 'auction_add_time', 'auction_start_time', 'auction_end_time', 'auction_image'], 'required'],
            [['auction_bond', 'auction_start_price', 'auction_reference_price', 'auction_increase_range', 'auction_reserve_price', 'current_price', 'auction_bond_rate', 'auction_cost', 'express_fee'], 'number'],
            [['interest_last_date'], 'safe'],
            [['auctions_spec', 'auctions_summary', 'other_images', 'desc_content'], 'string'],
            [['auction_name', 'store_name', 'auctions_artist_name'], 'string', 'max' => 100],
            [['auction_image', 'auction_video', 'delivery_mechanism'], 'string', 'max' => 255],
            [['create_age'], 'string', 'max' => 200],
            [['share_content'], 'string', 'max' => 500],
            [['warehouse_location'], 'string', 'max' => 50],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'auction_id' => '拍卖品ID',
            'goods_id' => '商品(SKU)id',
            'goods_common_id' => '商品id',
            'auction_name' => '拍品名称',
            'auction_type' => '拍品类型: 0,普通拍品,1,新手拍品 2.新手专区 3.捡漏拍品',
            'store_id' => '店铺ID',
            'store_name' => '店铺名称',
            'special_id' => '专场id',
            'auction_add_time' => '拍品添加时间',
            'auction_edit_time' => '拍品编辑时间',
            'auction_preview_start' => '预展开始时间',
            'auction_start_time' => '开始拍卖时间',
            'auction_end_time' => '结束拍卖时间',
            'auction_end_true_t' => '拍品真实结束时间',
            'auction_image' => '拍品主图',
            'auction_video' => '拍品视频',
            'auction_bond' => '拍品保证金',
            'auction_start_price' => '起拍价',
            'auction_reference_price' => '参考价',
            'auction_increase_range' => '加价幅度',
            'auction_reserve_price' => '保留价',
            'delivery_mechanism' => '送拍机构',
            'state' => '拍卖状态: 0未结束 1拍卖结束',
            'gc_id' => '拍卖分类',
            'auction_lock' => '拍品锁定，0：未锁定，1：锁定',
            'auction_click' => '拍品点击数量',
            'is_liupai' => '是否流拍，0：未流拍，1：流拍',
            'current_price' => '拍品当前价',
            'bid_number' => '出价次数',
            'num_of_applicants' => '报名人数',
            'real_participants' => '真实参与人数',
            'set_reminders_num' => '设置提醒人数',
            'auction_collect' => '收藏量',
            'recommended' => '推荐 1：推荐活动 0：普通活动',
            'current_price_time' => '当前价出价时间',
            'store_vendue_id' => '店铺拍卖表ID',
            'auction_bond_rate' => '保证金利息',
            'interest_last_date' => '最后计息日期',
            'auctions_artist_id' => '艺术家ID',
            'auctions_artist_name' => '艺术家名字',
            'auctions_class_lv1' => '一级分类',
            'auctions_class_lv2' => '二级分类',
            'auctions_spec' => '规格json',
            'auctions_long' => '长',
            'auctions_width' => '宽',
            'auctions_height' => '高',
            'create_age' => '创作年代',
            'auctions_summary' => '作品简介',
            'other_images' => '明细图片',
            'share_content' => '分享描述',
            'duration_time' => '持续时间',
            'warehouse_location' => '库位号,A12031:A区12号货架第31号库位',
            'auction_cost' => '拍品成本价格',
            'check_status' => '审核状态 10 未审核 20 审核成功 30审核失败',
            'is_open' => '是否开启 0:关闭 1:开启',
            'auction_sponsor' => '发起者 0:店铺 1:平台',
            'is_recommend' => '是否推荐 0:不推荐 1:推荐',
            'free_post' => '是否包邮 0:不包邮 1:包邮',
            'express_fee' => '快递费',
            'goods_storage' => '拍品库存',
            'desc_content' => '作品简介',
        ];
    }
}
