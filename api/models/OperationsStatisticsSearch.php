<?php

namespace api\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use api\models\OperationsStatistics;

/**
 * OperationsStatisticsSearch represents the model behind the search form of `api\models\OperationsStatistics`.
 */
class OperationsStatisticsSearch extends OperationsStatistics
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'source_id', 'order_sn', 'amount_type', 'amount_status'], 'integer'],
            [['commission_amount'], 'number'],
            [['amount_remarks', 'created_at', 'updated_at'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = OperationsStatistics::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'source_id' => $this->source_id,
            'order_sn' => $this->order_sn,
            'commission_amount' => $this->commission_amount,
            'amount_type' => $this->amount_type,
            'amount_status' => $this->amount_status,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
        ]);

        $query->andFilterWhere(['like', 'amount_remarks', $this->amount_remarks]);

        return $dataProvider;
    }
}
