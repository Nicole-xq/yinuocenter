<?php

namespace api\models;

use Yii;

/**
 * This is the model class for table "shopnc_member_distribute".
 *
 * @property int $dis_id 分销编号
 * @property int $member_id 会员编号
 * @property int $add_time 添加时间
 * @property string $dis_code 邀请码
 * @property string $member_mobile 会员手机号
 * @property int $top_member 上级会员编号
 * @property int $top_member_2 上级的上级会员编号
 * @property int $member_type 会员类型 0注册会员 1普通会员 2特一级会员 3特二级会员 4艺术家
 * @property string $member_name 会员名称
 * @property string $member_comment 用户备注(上级用户对下级用户的备注)
 * @property string $member_comment_2 用户备注(上上级用户对2级用户的备注)
 * @property string $top_member_name 上级会员名称
 * @property string $wx_qrcode_ticket 微信二维码ticket
 * @property string $wx_qrcode_url 二维码的URL
 * @property string $qr_expire_seconds 二维码过期时间：0不过期
 */
class ShopncMemberDistribute extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'shopnc_member_distribute';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['member_id', 'add_time'], 'required'],
            [['member_id', 'add_time', 'top_member', 'top_member_2', 'member_type', 'qr_expire_seconds'], 'integer'],
            [['dis_code'], 'string', 'max' => 50],
            [['member_mobile'], 'string', 'max' => 12],
            [['member_name', 'member_comment', 'member_comment_2', 'top_member_name', 'wx_qrcode_ticket', 'wx_qrcode_url'], 'string', 'max' => 255],
            [['member_id'], 'unique'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'dis_id' => '分销编号',
            'member_id' => '会员编号',
            'add_time' => '添加时间',
            'dis_code' => '邀请码',
            'member_mobile' => '会员手机号',
            'top_member' => '上级会员编号',
            'top_member_2' => '上级的上级会员编号',
            'member_type' => '会员类型 0注册会员 1普通会员 2特一级会员 3特二级会员 4艺术家',
            'member_name' => '会员名称',
            'member_comment' => '用户备注(上级用户对下级用户的备注)',
            'member_comment_2' => '用户备注(上上级用户对2级用户的备注)',
            'top_member_name' => '上级会员名称',
            'wx_qrcode_ticket' => '微信二维码ticket',
            'wx_qrcode_url' => '二维码的URL',
            'qr_expire_seconds' => '二维码过期时间：0不过期',
        ];
    }


    public function getMembers()
    {
        return $this->hasMany(ShopncMember::className(), ['top_member' => 'member_id']);
    }
}
