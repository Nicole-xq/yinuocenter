<?php

namespace api\controllers;

use api\models\ShopncMember;
use api\models\ShopncMemberDistributeSearch;
use Yii;
use api\models\ShopncOrders;
use api\models\ShopncOrdersSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\data\Pagination;

/**
 * OrdersController implements the CRUD actions for ShopncOrders model.
 */
class OrdersController extends BaseController
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * 获取当前运营商下交易记录
     *
     * @return mixed
     */
    public function actionIndex()
    {
        $params = Yii::$app->request->get();
        $top_member = $this->checkToken($params['token']);
        if (empty($params['token'])) {
            return $this->json($code = -1, $data = '', $message = '请求参数有误');
        }
        if (empty($top_member['member_id'])) {
            return $this->json($code = -1, $data = '', $message = '重新登录');
        }
        $top_member_id = $top_member['member_id'];
        $ShopncMemberDistributeSearch = new ShopncMemberDistributeSearch();
        //获取用户IDS
        $getMemberIds = $ShopncMemberDistributeSearch->getMemberIds2($top_member_id);

        $where_order = [
            'buyer_id' => $getMemberIds,
        ];

        $files = ['order_sn', 'buyer_id', 'buyer_name', 'buyer_phone', 'source_staff_id', 'order_amount', 'order_type', 'order_state', 'add_time', 'finnshed_time'];


        $query = ShopncOrders::find()->select($files)->where($where_order);

        //订单状态：0(已取消)10(默认):未付款;20:已付款;30:已发货;40:已收货;50:违约
        if (isset($params['order_state'])) {
            $where_order_state = [
                'order_state' => $params['order_state']
            ];
            $query = $query->andWhere($where_order_state);
        }
        //按买家手机搜索
        if (isset($params['buyer_phone'])) {
            $where_buyer_phone = [
                'buyer_phone' => $params['buyer_phone']
            ];
            $query = $query->andWhere($where_buyer_phone);
        }

        // 得到交易订单的总数（但是还没有从数据库取数据）
        $count = $query->count();

        // 使用总数来创建一个分页对象
        $pagination = new Pagination(['totalCount' => $count]);

        // 使用分页对象来填充 limit 子句并取得文章数据
        $BidLog = $query->offset($pagination->offset)
            ->limit($pagination->limit)
            ->asArray()
            ->all();
        foreach ($BidLog as $key => $value) {
            $ShopncMember = new ShopncMember();
            $getSourceStaff = $ShopncMember->getSourceStaff($value['source_staff_id']);
            $BidLog[$key]['source_staff_name'] = $getSourceStaff['member_name'];
            $BidLog[$key]['add_time'] = date("Y-m-d H:i:s", $value['add_time']);
            $BidLog[$key]['finnshed_time'] = date("Y-m-d H:i:s", $value['finnshed_time']);
        }
        return $this->json($code = 0, $data = $BidLog, $message = '获取交易列表成功');
    }


    /**
     * 获取当前运营商下订单奖励金记录
     *
     * @return mixed
     */
    public function actionCommission()
    {
        $params = Yii::$app->request->get();
        $top_member = $this->checkToken($params['token']);
        if (empty($params['token'])) {
            return $this->json($code = -1, $data = '', $message = '请求参数有误');
        }
        if (empty($top_member['member_id'])) {
            return $this->json($code = -1, $data = '', $message = '重新登录');
        }
        $top_member_id = $top_member['member_id'];
        $ShopncMemberDistributeSearch = new ShopncMemberDistributeSearch();
        //获取用户IDS
        $getMemberIds = $ShopncMemberDistributeSearch->getMemberIds2($top_member_id);

        $where_order = [
            'buyer_id' => $getMemberIds,
        ];

        $files = ['order_sn', 'buyer_id', 'buyer_name', 'buyer_phone', 'source_staff_id', 'order_amount', 'order_type', 'add_time', 'finnshed_time', 'auction_cost', 'commission_state', 'commission_amount'];

        //佣金结算状态 0未结 1已结
        if (isset($params['commission_state'])) {
            $where_order = [
                'buyer_id' => $getMemberIds,
                'commission_state' => $params['commission_state']
            ];
        }

        $query = ShopncOrders::find()->select($files)->where($where_order);

        //按结算时间搜索 当未结算时无结束时间
        if (isset($params['add_time']) && isset($params['finnshed_time']) && $params['commission_state']==1) {
            $within = [
                'and',
                ['>=', 'add_time', $params['add_time']],
                ['<=', 'finnshed_time', $params['finnshed_time']]
            ];
            $query = $query->andWhere($within);
        }


        // 得到交易订单的总数（但是还没有从数据库取数据）
        $count = $query->count();

        // 使用总数来创建一个分页对象
        $pagination = new Pagination(['totalCount' => $count]);

        // 使用分页对象来填充 limit 子句并取得文章数据
        $BidLog = $query->offset($pagination->offset)
            ->limit($pagination->limit)
            ->asArray()
            ->all();
        foreach ($BidLog as $key => $value) {
            $ShopncMember = new ShopncMember();
            $getSourceStaff = $ShopncMember->getSourceStaff($value['source_staff_id']);
            $BidLog[$key]['source_staff_name'] = $getSourceStaff['member_name'];
//            $BidLog[$key]['add_time'] = date("Y-m-d H:i:s", $value['add_time']);
//            $BidLog[$key]['finnshed_time'] = date("Y-m-d H:i:s", $value['finnshed_time']);
            //总成本=拍品成本
            $BidLog[$key]['order_cost'] = $value['auction_cost'] + $value['commission_amount'];
        }
        return $this->json($code = 0, $data = $BidLog, $message = '获取奖励金列表成功');
    }

    /**
     * Displays a single ShopncOrders model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }


    /**
     * Finds the ShopncOrders model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return ShopncOrders the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = ShopncOrders::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
