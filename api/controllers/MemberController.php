<?php

namespace api\controllers;

use api\models\ShopncMemberDistribute;
use api\models\ShopncMemberDistributeType;
use Yii;
use api\models\ShopncMember;
use api\models\ShopncMemberSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\data\Pagination;
use api\models\ShopncMemberDistributeSearch;
use api\models\ShopncOrders;

/**
 * MemberController implements the CRUD actions for ShopncMember model.
 */
class MemberController extends BaseController
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * 获取当前运营商下会员列表
     *
     * @return mixed
     */
    public function actionIndex()
    {
        $params = Yii::$app->request->get();
        $top_member = $this->checkToken($params['token']);
        if (empty($params['token'])) {
            return $this->json($code = -1, $data = '', $message = '请求参数有误');
        }
        if (empty($top_member['member_id'])) {
            return $this->json($code = -1, $data = '', $message = '重新登录');
        }
        $top_member_id = $top_member['member_id'];
        $ShopncMemberDistributeSearch = new ShopncMemberDistributeSearch();
        //获取用户IDS
        $getMemberIds = $ShopncMemberDistributeSearch->getMemberIds($top_member_id);

        $files = ['member_id', 'member_avatar', 'member_type', 'member_mobile', 'member_type', 'member_time', 'registered_source', 'source_staff_id', 'member_old_login_time'];

        $query = ShopncMember::find()->select($files)->where('member_id in (' . $getMemberIds . ')');

        // 得到文章的总数（但是还没有从数据库取数据）
        $count = $query->count();

        // 使用总数来创建一个分页对象
        $pagination = new Pagination(['totalCount' => $count]);

        // 使用分页对象来填充 limit 子句并取得文章数据
        $BidLog = $query->offset($pagination->offset)
            ->limit($pagination->limit)
            ->asArray()
            ->all();
        $getDistributeType = ShopncMemberDistributeType::find()->select(['id', 'name'])->indexBy('id')->asArray()->all();
        foreach ($BidLog as $key => $value) {
            $ShopncMember = new ShopncMember();
            $getSourceStaff = $ShopncMember->getSourceStaff($value['member_id']);
            $BidLog[$key]['source_staff_name'] = $getSourceStaff['member_name'];
            $BidLog[$key]['member_type_name'] = $getDistributeType[$value['member_type']]['name'] ?? null;
            $BidLog[$key]['order_amount'] = ShopncOrders::find()->where(['buyer_id' => $value['member_id']])->sum('order_amount');
            $BidLog[$key]['commission_amount'] = ShopncOrders::find()->where(['buyer_id' => $value['member_id']])->sum('commission_amount');
            $BidLog[$key]['member_time'] = date("Y-m-d H:i:s", $value['member_time']);
            $BidLog[$key]['member_old_login_time'] = date("Y-m-d H:i:s", $value['member_old_login_time']);
        }
        return $this->json($code = 0, $data = $BidLog, $message = 'success');
    }

    /**
     * Displays a single ShopncMember model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new ShopncMember model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new ShopncMember();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->member_id]);
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * 管理员设置,修改管理员登录密码
     *
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate()
    {
        $params = Yii::$app->request->get();
        $top_member = $this->checkToken($params['token']);
        if (empty($params['token']) || empty($params['member_passwd'])) {
            return $this->json($code = -1, $data = '', $message = '请求参数有误');
        }
        if (empty($top_member['member_id'])) {
            return $this->json($code = -1, $data = '', $message = '重新登录');
        }
        $status = ShopncMember::updateAll(array( 'member_passwd'=> md5($params['member_passwd']) ),array("member_id"=>$top_member['member_id']));
        if ($status) {
            return $this->json($code = 0, $data = '', $message = 'success');
        }
        return $this->json($code = -1, $data = '', $message = 'failure');
    }

    /**
     * Deletes an existing ShopncMember model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the ShopncMember model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return ShopncMember the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = ShopncMember::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
