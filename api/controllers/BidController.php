<?php

namespace api\controllers;

use api\models\ShopncAuctions;
use api\models\ShopncMember;
use api\models\ShopncMemberDistributeSearch;
use api\models\ShopncOrders;
use Yii;
use api\models\ShopncBidLog;
use api\models\ShopncBidLogSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\data\Pagination;

/**
 * BidController implements the CRUD actions for ShopncBidLog model.
 */
class BidController extends BaseController
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * 获取当前运营商下竞价|流拍记录
     * @return mixed
     */
    public function actionIndex()
    {
        $params = Yii::$app->request->get();
        $top_member = $this->checkToken($params['token']);
        if (empty($params['token'])) {
            return $this->json($code = -1, $data = '', $message = '请求参数有误');
        }
        if (empty($top_member['member_id'])) {
            return $this->json($code = -1, $data = '', $message = '重新登录');
        }
        $top_member_id = $top_member['member_id'];
        $ShopncMemberDistributeSearch = new ShopncMemberDistributeSearch();
        //获取用户IDS
        $getMemberIds = $ShopncMemberDistributeSearch->getMemberIds2($top_member_id);

        //区分竞价和流拍
        $ShopncBidLog = new ShopncBidLog();
        $getAuctionsIds = $ShopncBidLog->find()->select(['auction_id'])->where(['member_id' => $getMemberIds])->asArray()->all();
        $getAuctionsIds = array_column($getAuctionsIds, 'auction_id');

        //流拍条件
        $is_liupai = $params['is_liupai']??0;
        $where = [
            'auction_id' => $getAuctionsIds,
            'is_liupai' => $is_liupai
        ];
        $getAuctionsData = ShopncAuctions::find()->select(['auction_id'])->where($where)->asArray()->all();
        $action_bid = array_column($getAuctionsData, 'auction_id');

        $where_bid = [
            'member_id' => $getMemberIds,
            'auction_id' => $action_bid,
        ];

        $files = ['bid_id', 'member_id', 'auction_id', 'offer_num', 'member_name', 'commission_amount', 'created_at'];

        $query = ShopncBidLog::find()->select($files)->where($where_bid);

        // 得到竞价记录的总数（但是还没有从数据库取数据）
        $count = $query->count();

        // 使用总数来创建一个分页对象
        $pagination = new Pagination(['totalCount' => $count]);

        // 使用分页对象来填充 limit 子句并取得文章数据
        $BidLog = $query->offset($pagination->offset)
            ->limit($pagination->limit)
            ->asArray()
            ->all();
        foreach ($BidLog as $key => $value) {
            //补充用户信息
            $ShopncMember = new ShopncMember();
            $ShopncMemberData = $ShopncMember->find()->select(['member_mobile', 'source_staff_id'])->where(['member_id' => $value['member_id']])->asArray()->one();
            $getSourceStaff = $ShopncMember->getSourceStaff($ShopncMemberData['source_staff_id']);
            $BidLog[$key]['member_mobile'] = $ShopncMemberData['member_mobile'];
            $BidLog[$key]['source_staff_name'] = $getSourceStaff['member_name'];
            //补充拍品信息
            $ShopncAuctions = new ShopncAuctions();
            $ShopncAuctionsData = $ShopncAuctions->find()->select(['auction_name', 'auction_increase_range'])->where(['auction_id' => $value['auction_id']])->asArray()->one();
            $BidLog[$key]['auction_name'] = $ShopncAuctionsData['auction_name'];
            $BidLog[$key]['auction_increase_range'] = $ShopncAuctionsData['auction_increase_range'];

            $BidLog[$key]['created_at'] = date("Y-m-d H:i:s", $value['created_at']);
        }
        return $this->json($code = 0, $data = $BidLog, $message = 'success');
    }

    /**
     * Displays a single ShopncBidLog model.
     * @param string $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }


    /**
     * Finds the ShopncBidLog model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param string $id
     * @return ShopncBidLog the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = ShopncBidLog::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
