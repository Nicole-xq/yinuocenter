<?php

namespace api\controllers;

use Yii;
use api\models\OperationsStatistics;
use api\models\OperationsStatisticsSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\data\Pagination;

/**
 * StatisticsController implements the CRUD actions for OperationsStatistics model.
 */
class StatisticsController extends BaseController
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * 获取当前运营商下资金管理列表
     *
     * @return mixed
     */
    public function actionIndex()
    {
        $params = Yii::$app->request->get();
        $top_member = $this->checkToken($params['token']);
        if (empty($params['token'])) {
            return $this->json($code = -1, $data = '', $message = '请求参数有误');
        }
        if (empty($top_member['member_id'])) {
            return $this->json($code = -1, $data = '', $message = '重新登录');
        }

        $condition = [
            'source_id' => $top_member['member_id'],
        ];
        //资金类别(1入账2出账)
        if (isset($params['amount_type'])) {
            $condition = [
                'source_id' => $top_member['member_id'],
                'amount_type' => $params['amount_type']
            ];
        }

        $query = OperationsStatistics::find()->where($condition);

        //按时间搜索
        if (isset($params['created_at']) && isset($params['updated_at'])) {
            $within = [
                'and',
                ['>=', 'created_at', $params['created_at']],
                ['<=', 'updated_at', $params['updated_at']]
            ];
            $query = $query->andWhere($within);
        }

        // 得到资金列表的总数（但是还没有从数据库取数据）
        $count = $query->count();

        // 使用总数来创建一个分页对象
        $pagination = new Pagination(['totalCount' => $count]);

        // 使用分页对象来填充 limit 子句并取得文章数据
        $BidLog = $query->offset($pagination->offset)
            ->limit($pagination->limit)
            ->asArray()
            ->all();
        return $this->json($code = 0, $data = $BidLog, $message = 'success');
    }

    /**
     * 获取当前运营商下资金流水详情
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView()
    {
        $params = Yii::$app->request->get();
        $top_member = $this->checkToken($params['token']);
        if (empty($params['token']) || empty($params['id'])) {
            return $this->json($code = -1, $data = '', $message = '请求参数有误');
        }
        if (empty($top_member['member_id'])) {
            return $this->json($code = -1, $data = '', $message = '重新登录');
        }
        $condition = [
            'id' => $params['id'],
        ];
        $query = OperationsStatistics::find()->where($condition)->asArray()->one();
        if ($query) {
            return $this->json($code = 0, $data = $query, $message = 'success');
        }
        return $this->json($code = -1, $data = '', $message = 'failure');
    }

    /**
     * Creates a new OperationsStatistics model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new OperationsStatistics();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing OperationsStatistics model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing OperationsStatistics model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the OperationsStatistics model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return OperationsStatistics the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = OperationsStatistics::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
