<?php

namespace api\controllers;

use api\models\OperationsStatistics;
use api\models\ShopncMember;
use Yii;
use api\models\OperationsAgent;
use api\models\OperationsAgentSearch;
use yii\data\Pagination;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\base\ErrorException;

/**
 * AgentController implements the CRUD actions for OperationsAgent model.
 */
class AgentController extends BaseController
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * 获取当前运营商下代理和员工
     *
     * @return mixed
     */
    public function actionIndex()
    {
        $params = Yii::$app->request->get();
        $top_member = $this->checkToken($params['token']);
        if (empty($params['token']) || empty($params['agent_type'])) {
            return $this->json($code = -1, $data = '', $message = '请求参数有误');
        }
        if (empty($top_member['member_id'])) {
            return $this->json($code = -1, $data = '', $message = '重新登录');
        }

        $condition = [
            'source_id' => $top_member['member_id'],
            'agent_type' => $params['agent_type']
        ];

        //搜索
        if (isset($params['keyword'])) {
            $condition = [
                'source_id' => $top_member['member_id'],
                'agent_type' => $params['agent_type'],
                'agent_phone' => $params['keyword'],
            ];
        }

        $query = OperationsAgent::find()->where($condition);

        // 得到代理的总数（但是还没有从数据库取数据）
        $count = $query->count();

        // 使用总数来创建一个分页对象
        $pagination = new Pagination(['totalCount' => $count]);

        // 使用分页对象来填充 limit 子句并取得文章数据
        $BidLog = $query->offset($pagination->offset)
            ->limit($pagination->limit)
            ->asArray()
            ->all();
        return $this->json($code = 0, $data = $BidLog, $message = 'success');
    }

    /**
     * Displays a single OperationsAgent model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * 新增代理|员工
     *
     * @return mixed
     */
    public function actionCreate()
    {
        $params = Yii::$app->request->get();
        $top_member = $this->checkToken($params['token']);
        if (empty($params['token']) || empty($params['agent_name']) || empty($params['agent_phone'])) {
            return $this->json($code = -1, $data = '', $message = '请求参数有误');
        }
        if (empty($top_member['member_id'])) {
            return $this->json($code = -1, $data = '', $message = '重新登录');
        }

        if ($params['agent_type'] == 1) {
            if(empty($params['commission_rate'])){
                return $this->json($code = -1, $data = '', $message = '缺少必要参数');
            }
            $commission_rate = $params['commission_rate'];
        }

        $model = new OperationsAgent();
        $params = Yii::$app->request->get();
        $model->agent_type = $params['agent_type'];
        $model->source_id = $top_member['member_id'];
        $model->agent_name = $params['agent_name'];
        $model->agent_phone = $params['agent_phone'];
        $model->commission_rate = $commission_rate??0;
        $model->agent_status = 1;
        if ($model->save()) {
            return $this->json($code = 0, $data = ['agent_id' => $model->agent_id], $message = 'success');
        }
        return $this->json($code = -1, $data = '', $message = 'failure');
    }

    /**
     * Updates an existing OperationsAgent model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->agent_id]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing OperationsAgent model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the OperationsAgent model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return OperationsAgent the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = OperationsAgent::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
