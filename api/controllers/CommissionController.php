<?php

namespace api\controllers;

use api\models\ShopncMember;
use api\models\ShopncMemberDistributeSearch;
use api\models\ShopncOrders;
use Yii;
use api\models\ShopncMemberCommission;
use api\models\ShopncMemberCommissionSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\data\Pagination;

/**
 * CommissionController implements the CRUD actions for ShopncMemberCommission model.
 */
class CommissionController extends BaseController
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * 获取当前运营商下奖励金列表
     *
     * @return mixed
     */
    public function actionIndex()
    {
        $params = Yii::$app->request->get();
        $top_member = $this->checkToken($params['token']);
        if(empty($params['token'])){
            return $this->json($code = -1, $data = '', $message = '请求参数有误');
        }
        if(empty($top_member['member_id'])){
            return $this->json($code = -1, $data = '', $message = '重新登录');
        }
        $top_member_id = $top_member['member_id'];
        $ShopncMemberDistributeSearch = new ShopncMemberDistributeSearch();
        //获取用户IDS
        $getMemberIds = $ShopncMemberDistributeSearch->getMemberIds($top_member_id);

        $files = ['order_sn','from_member_id','from_member_name','source_staff_id','commission_type','add_time','commission_amount','commission_time'];

        $query = ShopncMemberCommission::find()->select($files)->where('from_member_id in ('.$getMemberIds.')');

        // 得到奖励金列表的总数（但是还没有从数据库取数据）
        $count = $query->count();

        // 使用总数来创建一个分页对象
        $pagination = new Pagination(['totalCount' => $count]);

        // 使用分页对象来填充 limit 子句并取得文章数据
        $BidLog = $query->offset($pagination->offset)
            ->limit($pagination->limit)
            ->asArray()
            ->all();
        foreach ($BidLog as $key => $value) {
            $ShopncMember = new ShopncMember();
            $getSourceStaff = $ShopncMember->getSourceStaff($value['source_staff_id']);
            $BidLog[$key]['source_staff_name'] =$getSourceStaff['member_name'];
            $BidLog[$key]['add_time'] = date( "Y-m-d H:i:s", $value['add_time']);
            $BidLog[$key]['commission_time'] = date( "Y-m-d H:i:s", $value['commission_time']);
        }
        return $this->json($code = 0, $data = $BidLog, $message = 'success');
    }

    /**
     * Displays a single ShopncMemberCommission model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new ShopncMemberCommission model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new ShopncMemberCommission();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->log_id]);
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing ShopncMemberCommission model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->log_id]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing ShopncMemberCommission model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the ShopncMemberCommission model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return ShopncMemberCommission the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = ShopncMemberCommission::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
