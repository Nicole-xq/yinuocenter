<?php

use \yii\db\Migration;

class m190124_110200_add_verification_token_column_to_operations_user_table extends Migration
{
    public function up()
    {
        $this->addColumn('{{operations_user}}', 'verification_token', $this->string()->defaultValue(null));
    }

    public function down()
    {
        $this->dropColumn('{{operations_user}}', 'verification_token');
    }
}
